def __virtual__(hub):
    return getattr(hub, "LOAD_FAIL", False), "Plugin not enabled"


def args(hub, **__):
    ...


def kwargs(hub, *_):
    ...


def args_kwargs(hub):
    ...


def async_func(hub):
    ...
