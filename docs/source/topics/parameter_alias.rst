Parameter Aliases
=================

Parameters can have an alias.
This is useful when a parameter name clashes with a builtin python keyword(i.e. list, dict, id, type).
To give a parameter an alias, add "alias=<parameter_alias>" as it's type hint.

I.E.

.. code-block:: python

    def my_func(hub, id_: "alias=id"):
        ...

This function can now be called one of three ways:

  - With a positional argument: `hub._.my_func("value")`
  - With the parameter as defined `hub._.my_func(id_="value")`
  - With the the paramater alias `hub._.my_func(id="value")`

Whether "value" is passed as a positional argument, as a keyword argument, or with the keyword argument alias,
it will populate the "id\_" variable.

If the variable has a regular type-hint, you can make the whole parameter's type hint a tuple.
The first value in the tuple is the traditional `type` of the parameter.
The second value is a string that contains the alias notation.

I.E.

.. code-block:: python

    def my_func(hub, id_: (str, "alias=id")):
        ...


Multiple aliases can be defined for a single parameter.
Either as a tuple:

.. code-block:: python


    def my_func(hub, id_: ("alias=id", "alias=other")):
        ...


Or in a single string:

.. code-block:: python


    def my_func(hub, id_: "alias=id, alias=other"):
        ...

If aliases conflict with other arguments, or if the same alias is used for multiple parameters in the same function,
then a SyntaxError will be thrown.


Retrieving Param Aliases
------------------------

Parameter Aliases are part of the contracted object that represent a function.
With the function ref on the hub, you can access the aliases available to a function:

.. code-block:: python


    def my_func(hub, id_: "alias=id, alias=other"):
        ...


    hub._.my_func.param_aliases == {"id_": {"id_", "id", "other"}}
